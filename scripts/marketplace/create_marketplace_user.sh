#!/bin/bash

echo "Create users ..."
curl -v -H "Content-Type: application/xml" -X PUT --data "@msg_create_marketplace_user.xml" -u "demo1234:demo1234" http://localhost:8080/FiwareMarketplace/v1/registration/userManagement/user

echo "Test created user ..."
# assumes that testStore123 has been created
curl -v -H "Content-Type: application/xml" -X GET -u "store_conwet:store_conwet" http://localhost:8080/FiwareMarketplace/v1/registration/store/testName123
